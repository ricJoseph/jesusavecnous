import 'package:flutter/material.dart';
import 'dimensions.dart';

class ResponsiveLayout extends StatefulWidget {
  final Widget mobileBody;
  final Widget tabletBody;
  final Widget desktopBody;
  const ResponsiveLayout(
      {super.key,
      required this.mobileBody,
      required this.tabletBody,
      required this.desktopBody});

  @override
  State<ResponsiveLayout> createState() => _ResponsiveLayoutState();
}

class _ResponsiveLayoutState extends State<ResponsiveLayout> {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth < mobileWidth) {
          return widget.mobileBody;
        } else {
          if (constraints.maxWidth >= mobileWidth &&
              constraints.maxWidth < tabletWigth) {
            return widget.tabletBody;
          } else {
            return widget.desktopBody;
          }
        }
      },
    );
  }
}
