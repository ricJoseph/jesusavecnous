import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

Color primaryColor = const Color.fromRGBO(245, 73, 0, 1);
Color secondaryColor = const Color.fromRGBO(105, 35, 2, 1);

Color primaryText = Colors.black;
Color secondaryText = Colors.white;

Color primaryborder = const Color(0xFFD3D3D3);

// --------------------

class Description extends StatelessWidget {
  final String text;
  final Color color;
  final double size;
  const Description(
      {super.key, required this.text, required this.color, required this.size});

  @override
  Widget build(BuildContext context) {
    return Text(text,
        textAlign: TextAlign.justify,
        style: GoogleFonts.nunito(
          fontSize: size,
          fontWeight: FontWeight.w500,
          color: color,
          letterSpacing: 2,
        ));
  }
}
