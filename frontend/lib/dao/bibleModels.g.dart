// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bibleModels.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ATest _$ATestFromJson(Map<String, dynamic> json) => ATest()
  ..label = json['label'] as String
  ..at = (json['at'] as List<dynamic>)
      .map((e) => Book.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$ATestToJson(ATest instance) => <String, dynamic>{
      'label': instance.label,
      'at': instance.at,
    };

NTest _$NTestFromJson(Map<String, dynamic> json) => NTest()
  ..label = json['label'] as String
  ..nt = (json['nt'] as List<dynamic>)
      .map((e) => Book.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$NTestToJson(NTest instance) => <String, dynamic>{
      'label': instance.label,
      'nt': instance.nt,
    };

Book _$BookFromJson(Map<String, dynamic> json) => Book()
  ..num = json['num'] as String
  ..name = json['name'] as String
  ..sname = json['sname'] as String
  ..chapitres =
      (json['chapitres'] as List<dynamic>).map((e) => e as String).toList();

Map<String, dynamic> _$BookToJson(Book instance) => <String, dynamic>{
      'num': instance.num,
      'name': instance.name,
      'sname': instance.sname,
      'chapitres': instance.chapitres,
    };

Chapter _$ChapterFromJson(Map<String, dynamic> json) => Chapter()
  ..num = json['num'] as String
  ..versets =
      (json['versets'] as List<dynamic>).map((e) => e as String).toList();

Map<String, dynamic> _$ChapterToJson(Chapter instance) => <String, dynamic>{
      'num': instance.num,
      'versets': instance.versets,
    };

Verset _$VersetFromJson(Map<String, dynamic> json) => Verset()
  ..num = json['num'] as String
  ..data = json['data'] as String;

Map<String, dynamic> _$VersetToJson(Verset instance) => <String, dynamic>{
      'num': instance.num,
      'data': instance.data,
    };

Versets _$VersetsFromJson(Map<String, dynamic> json) => Versets()
  ..versets = (json['versets'] as List<dynamic>)
      .map((e) => Verset.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$VersetsToJson(Versets instance) => <String, dynamic>{
      'versets': instance.versets,
    };
