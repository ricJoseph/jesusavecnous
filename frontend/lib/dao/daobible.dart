import 'package:http/http.dart' as http;
import 'package:frontend/dao/bibleModels.dart';
import 'dart:convert';
import 'dart:async';

//function to get the 2 part of the bible
Future<List<Book>> getPart(String n) async {
  List<Book> defaultValue = [Book()];
  try {
    http.Response result = await http.get(
        Uri.parse('http://localhost:8000/manager/getPart$n'),
        headers: {'Accept': 'application/json; charset-UTF-8'});
    if (result.statusCode == 200) {
      Map<String, dynamic> test = jsonDecode(result.body);
      if (n == "A") {
        var response = ATest.fromJson(jsonDecode(test['message']));
        return response.at;
      } else {
        var response = NTest.fromJson(jsonDecode(test['message']));
        return response.nt;
      }
    } else {
      return defaultValue;
    }
  } catch (e) {
    print(e.toString());
  }

  return defaultValue;
}

Future<List<Verset>> getVesets(String chapterId) async {
  List<Verset> response = [];
  try {
    http.Response result = await http.get(
        Uri.parse('http://localhost:8000/manager/getVersets')
            .replace(queryParameters: {'chapterId': chapterId}),
        headers: {'Accept': 'application/json; charset-UTF-8'});
    if (result.statusCode == 200) {
      var tes = jsonDecode(result.body);
      response = (jsonDecode(tes) as List).map((data) {
        return Verset.fromJson(data);
      }).toList();
      return response;
    } else {
      return response;
    }
  } catch (e) {
    print(e.toString());
  }
  return response;
}
