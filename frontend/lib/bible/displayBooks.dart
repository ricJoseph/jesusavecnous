import 'package:flutter/material.dart';
import 'package:frontend/dao/daobible.dart';
import 'package:frontend/dao/bibleModels.dart';
import 'package:frontend/routes.dart';
import 'package:frontend/theme.dart';
import 'package:google_fonts/google_fonts.dart';

class DisplayBooks extends StatefulWidget {
  const DisplayBooks({
    super.key,
    required this.argmt,
    required this.onDisplayBooks,
  });

  final void Function(List<String> booksIds, String bookName, int bookColor) onDisplayBooks;
  final BibleBookChoiceInt argmt;

  @override
  State<DisplayBooks> createState() => _DisplayBooksState();
}

class _DisplayBooksState extends State<DisplayBooks> {
  late Future<List<Book>> ancienTestament;
  late Future<List<Book>> nouveauTestament;
  late int bookColor;

  @override
  void initState() {
    super.initState();
    ancienTestament = getPart('A');
    nouveauTestament = getPart('N');
    bookColor = widget.argmt.bookCoor;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          bookColor = 0;
                        });
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor:
                            bookColor == 0 ? primaryColor : Colors.white,
                        elevation: 0,
                      ),
                      child: Text("Ancien Testament",
                          style: GoogleFonts.nunito(
                              color: primaryText,
                              fontWeight: FontWeight.w300,
                              fontSize: 15,
                              letterSpacing: 3))),
                  ElevatedButton(
                      onPressed: () {
                        setState(() {
                          bookColor = 1;
                        });
                      },
                      style: ElevatedButton.styleFrom(
                          elevation: 0,
                          backgroundColor:
                              bookColor == 1 ? primaryColor : Colors.grey[100]),
                      child: Text("Nouveau Testament",
                          style: GoogleFonts.nunito(
                              color: primaryText,
                              fontWeight: FontWeight.w300,
                              fontSize: 15,
                              letterSpacing: 3)))
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              FutureBuilder<List<Book>>(
                future: bookColor == 0 ? ancienTestament : nouveauTestament,
                builder: (context, snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return const CircularProgressIndicator();
                  } else if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasError) {
                      String? test = snapshot.error.toString();
                      // print(test);
                      return const Description(
                        text: "There was a problem ",
                        color: Colors.redAccent,
                        size: 15,
                      );
                    } else if (snapshot.hasData) {
                      return ListView.builder(
                        shrinkWrap: true,
                        //scrollDirection: Axis.vertical,
                        itemCount: snapshot.data?.length,
                        itemBuilder: (context, index) {
                          return InkWell(
                            focusColor: Colors.orange[200],
                            onTap: () {
                              widget.onDisplayBooks(
                                  snapshot.data![index].chapitres,
                                  snapshot.data![index].name,
                                  bookColor);
                            },
                            child: DisplayOneBook(snapshot.data![index]),
                          );
                        },
                      );
                    } else {
                      return const Description(
                        text: "Empty Data",
                        color: Colors.redAccent,
                        size: 15,
                      );
                    }
                  } else {
                    return Description(
                      text: "State : ${snapshot.connectionState}",
                      color: Colors.redAccent,
                      size: 15,
                    );
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}

class DisplayOneBook extends StatelessWidget {
  final Book book;
  DisplayOneBook(this.book);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: primaryborder))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            margin: const EdgeInsets.fromLTRB(0, 15, 0, 15),
            child: Text(book.name.toString(),
                style: GoogleFonts.nunito(
                    color: primaryText,
                    fontWeight: FontWeight.w500,
                    fontSize: 15,
                    letterSpacing: 3)),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 15, 0, 15),
            child: Text("(${book.sname.toString()})",
                style: GoogleFonts.nunito(
                    color: primaryText,
                    fontWeight: FontWeight.w500,
                    fontSize: 15,
                    letterSpacing: 3)),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(0, 15, 0, 15),
            child: Icon(
              Icons.chevron_right,
              color: primaryColor,
              size: 25,
            ),
          ),
        ],
      ),
    );
  }
}
