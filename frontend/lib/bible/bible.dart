import 'package:flutter/material.dart';
import 'package:frontend/theme.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:frontend/routes.dart';
import 'package:frontend/bible/displayBooks.dart';
import 'package:frontend/bible/displayChapters.dart';
import 'package:frontend/bible/displayOneChapter.dart';
import 'package:frontend/bible/displaySearch.dart';
import 'package:frontend/bible/displaySearchDisplayItem.dart';
import 'package:frontend/bible/displaySearchResult.dart';

@immutable
class BibleFlow extends StatefulWidget {
  static BibleFlowState of(BuildContext context) {
    return context.findAncestorStateOfType<BibleFlowState>()!;
  }

  const BibleFlow({
    super.key,
    required this.biblePageRoute,
  });

  final String biblePageRoute;

  @override
  BibleFlowState createState() => BibleFlowState();
}

class BibleFlowState extends State<BibleFlow> {
  static final _navigatorKey = GlobalKey<NavigatorState>();

  @override
  void initSate() {
    super.initState();
  }

  void _onDisplayBooks(List<String> chaptersIds, String bookName, int bookColor) {
    _navigatorKey.currentState!.pushNamed(routeBibleChapterList,
        arguments: BibleStringArguments('chapters', chaptersIds, bookName, bookColor));
  }

  void _onDisplayChapters(
      String versetsId, String bookName, List<String> chapterIds, int pos, int bookColor) {
    _navigatorKey.currentState!.pushNamed(routeBibleChapterPage,
        arguments: BibleVersetsArguments(
            'versets', versetsId, bookName, chapterIds, pos, bookColor));
  }

  void _onDisplayChaptersPreviousNext(
      String versetsId, String bookName, List<String> chapterIds, int pos, int bookColor) {
    _navigatorKey.currentState!.pushNamed(routeBibleChapterPage,
        arguments: BibleVersetsArguments(
            'versets', versetsId, bookName, chapterIds, pos, bookColor));
  }

  void _onDisplayFinished() {
    _navigatorKey.currentState!.pushNamed(routeBibleBooks);
  }

  void _onDisplayBackToBooks(int book) {
    _navigatorKey.currentState!
        .pushNamed(routeBibleBooksPage, arguments: BibleBookChoiceInt(book));
  }

  void _onDisplaySearch() {
    _navigatorKey.currentState!.pushNamed(routeBibleSearchDisplay);
  }

  void _onDisplaySearchDisplay(String searchRequest) {
    _navigatorKey.currentState!.pushNamed(routeBibleSearchDisplayItem);
  }

  Future<void> _onExtSearchPressed() async {
    final isConfirmed = await _isExitDesired();

    if (isConfirmed && mounted) {
      _exitSearch();
    }
  }

  Future<bool> _isExitDesired() async {
    return await showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Description(
                  text: 'Are you Sure',
                  color: primaryText,
                  size: 20,
                ),
                content: Description(
                  text: 'Do you want to Leave this page ?',
                  color: primaryText,
                  size: 15,
                ),
                actions: [
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop(true);
                      },
                      child: Description(
                        text: 'Leave',
                        color: primaryText,
                        size: 13,
                      )),
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                      child: Description(
                        text: "Stay",
                        color: primaryText,
                        size: 13,
                      ))
                ],
              );
            }) ??
        false;
  }

  void _exitSearch() {
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: _isExitDesired,
        child: Scaffold(
          appBar: _buildBilbleFlowAppBar(),
          body: Navigator(
            key: _navigatorKey,
            initialRoute: widget.biblePageRoute,
            onGenerateRoute: _onGeneratedBibleRoute,
          ),
        ));
  }

  Route _onGeneratedBibleRoute(RouteSettings settings) {
    Widget page = Container();

    switch (settings.name) {
      case routeBibleBooksPage:
        final args = (settings.arguments == null)
            ? BibleBookChoiceInt(0)
            : settings.arguments as BibleBookChoiceInt;
        page = DisplayBooks(
          argmt: args,
          onDisplayBooks: _onDisplayBooks,
        );
        break;
      case routeBibleChapterList:
        final args = settings.arguments as BibleStringArguments;
        page = DisplayChaters(
          argmts: args,
          onDisplayChapters: _onDisplayChapters,
        );
        break;
      case routeBibleChapterPage:
        final args = settings.arguments as BibleVersetsArguments;
        page = DisplayOneChapter(
          argmts: args,
          onDisplayBackToBooks: _onDisplayBackToBooks,
          ondisplayChapterPreviousNext: _onDisplayChaptersPreviousNext,
          onDisplayFinished: _onDisplayFinished,
        );
        break;
      case routeBibleSearch:
        page = DisplaySearch();
        break;
      case routeBibleSearchDisplay:
        page = DisplaySearchDisplay();
        break;
      case routeBibleSearchDisplayItem:
        page = DisplaySearchDisplayItem();
        break;
    }

    return MaterialPageRoute<dynamic>(
      builder: (context) {
        return page;
      },
      settings: settings,
    );
  }

  PreferredSizeWidget _buildBilbleFlowAppBar() {
    return AppBar(
      
      toolbarHeight: 50,
      backgroundColor: primaryColor,
      elevation: 0,
      centerTitle: true,
      title: Text("La Sainte Bible",
          style: GoogleFonts.nunito(
              color: primaryText,
              fontWeight: FontWeight.w500,
              fontSize: 20,
              letterSpacing: 4)),
      actions: [
        IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.nightlight_round,
              color: primaryText,
            )),
        IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.search,
              color: primaryText,
            )),
      ],
    );
  }
}
