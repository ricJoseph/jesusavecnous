import 'package:flutter/material.dart';
import 'package:frontend/routes.dart';
import 'package:frontend/theme.dart';

class DisplayChaters extends StatefulWidget {
  const DisplayChaters(
      {super.key, required this.onDisplayChapters, required this.argmts});
  final BibleStringArguments argmts;
  final Function(
          String chapterId, String bookName, List<String> chapterIds, int pos, int bookColor)
      onDisplayChapters;

  @override
  State<DisplayChaters> createState() => _DisplayChatersState();
}

class _DisplayChatersState extends State<DisplayChaters> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemCount: widget.argmts.message.length,
              itemBuilder: (BuildContext context, int index) {
                return InkWell(
                  focusColor: Colors.orange[200],
                  onTap: () {
                    widget.onDisplayChapters(widget.argmts.message[index],
                        widget.argmts.bookName, widget.argmts.message, index, widget.argmts.bookColor);
                  },
                  child: Container(
                    alignment: Alignment.center,
                    padding: const EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                        border:
                            Border(bottom: BorderSide(color: primaryborder))),
                    child: Description(
                      text: 'Chapitre ${index + 1}',
                      color: primaryText,
                      size: 15,
                    ),
                  ),
                );
              },
            )),
      ),
    );
  }
}
