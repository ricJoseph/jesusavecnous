import 'package:flutter/material.dart';
import 'package:frontend/routes.dart';
import 'package:frontend/dao/daobible.dart';
import 'package:frontend/dao/bibleModels.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:frontend/theme.dart';

class DisplayOneChapter extends StatefulWidget {
  const DisplayOneChapter(
      {super.key,
      required this.argmts,
      required this.onDisplayBackToBooks,
      required this.ondisplayChapterPreviousNext,
      required this.onDisplayFinished});

  final BibleVersetsArguments argmts;
  final VoidCallback onDisplayFinished;
  final Function(int book) onDisplayBackToBooks;
  final Function(String chapterId, String bookName, List<String> chapterIds,
      int pos, int bookColor) ondisplayChapterPreviousNext;

  @override
  State<DisplayOneChapter> createState() => _DisplayOneChapterState();
}

class _DisplayOneChapterState extends State<DisplayOneChapter> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  child: Icon(
                    Icons.skip_previous,
                    color: primaryColor,
                  ),
                  onTap: () {
                    widget.ondisplayChapterPreviousNext(
                        (widget.argmts.pos < 0)
                            ? widget.argmts.chaptersIds[widget.argmts.pos]
                            : widget.argmts.chaptersIds[widget.argmts.pos - 1],
                        widget.argmts.bookName,
                        widget.argmts.chaptersIds,
                        widget.argmts.pos - 1,
                        widget.argmts.bookColor);
                  },
                ),
                ElevatedButton(
                    onPressed: () {
                      widget.onDisplayBackToBooks(0);
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: (widget.argmts.bookColor == 0)
                          ? primaryColor
                          : Colors.grey[100],
                      elevation: 0,
                    ),
                    child: (widget.argmts.bookColor == 0) ? 
                    Column(
                      children: [
                        Text("Ancien Testament",
                            style: GoogleFonts.nunito(
                                color: primaryText,
                                fontWeight: FontWeight.w300,
                                fontSize: 12,
                                letterSpacing: 3)),
                        Text('${widget.argmts.bookName} Chapitre ${widget.argmts.pos + 1}',
                            style: GoogleFonts.nunito(
                                color: primaryText,
                                fontWeight: FontWeight.w300,
                                fontSize: 10,
                                letterSpacing: 3)),
                      ],
                    )
                    : 
                    Text("Ancien Testament",
                            style: GoogleFonts.nunito(
                                color: primaryText,
                                fontWeight: FontWeight.w300,
                                fontSize: 15,
                                letterSpacing: 3))
                    ),
                ElevatedButton(
                    onPressed: () {
                      widget.onDisplayBackToBooks(1);
                    },
                    style: ElevatedButton.styleFrom(
                      elevation: 0,
                      backgroundColor: (widget.argmts.bookColor == 1)
                          ? primaryColor
                          : Colors.grey[100],
                    ),
                    child: (widget.argmts.bookColor == 1) ? 
                    Column(
                      children: [
                        Text("Nouveau Testament",
                            style: GoogleFonts.nunito(
                                color: primaryText,
                                fontWeight: FontWeight.w300,
                                fontSize: 12,
                                letterSpacing: 3)),
                        Text('${widget.argmts.bookName} Chapitre ${widget.argmts.pos + 1}',
                            style: GoogleFonts.nunito(
                                color: primaryText,
                                fontWeight: FontWeight.w300,
                                fontSize: 10,
                                letterSpacing: 3)),
                      ],
                    ) : 
                    Text("Nouveau Testament",
                            style: GoogleFonts.nunito(
                                color: primaryText,
                                fontWeight: FontWeight.w300,
                                fontSize: 15,
                                letterSpacing: 3))),
                InkWell(
                  child: Icon(
                    Icons.skip_next,
                    color: primaryColor,
                  ),
                  onTap: () {
                    widget.ondisplayChapterPreviousNext(
                        (widget.argmts.pos > widget.argmts.chaptersIds.length)
                            ? widget.argmts.chaptersIds[widget.argmts.pos]
                            : widget.argmts.chaptersIds[widget.argmts.pos + 1],
                        widget.argmts.bookName,
                        widget.argmts.chaptersIds,
                        widget.argmts.pos + 1,
                        widget.argmts.bookColor);
                  },
                ),
              ],
            ),
            const SizedBox(
              height: 5,
            ),
            FutureBuilder<List<Verset>>(
              future: getVesets(widget.argmts.message),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                } else if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    return const Description(
                        text: "A problem Occured",
                        color: Colors.redAccent,
                        size: 15);
                  } else if (snapshot.hasData) {
                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount: snapshot.data?.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          focusColor: Colors.orange[200],
                          onTap: () {},
                          child: DisplayOneVerset(snapshot.data![index]),
                        );
                      },
                    );
                  } else {
                    return const Description(
                        text: "A problem Occured",
                        color: Colors.redAccent,
                        size: 15);
                  }
                } else {
                  return const Description(
                      text: "Try Aiggain Later",
                      color: Colors.redAccent,
                      size: 15);
                }
              },
            )
          ]),
        ),
      ),
    );
  }
}

class DisplayOneVerset extends StatelessWidget {
  final Verset verset;
  const DisplayOneVerset(this.verset);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border(bottom: BorderSide(color: primaryborder))),
      child: VersetDescription(
        color: primaryText,
        text: verset.data,
        size: 15,
        versetNumber: verset.num,
      ),
    );
  }
}

class VersetDescription extends StatelessWidget {
  final String text;
  final Color color;
  final double size;
  final String versetNumber;

  const VersetDescription(
      {super.key,
      required this.text,
      required this.color,
      required this.size,
      required this.versetNumber});

  @override
  Widget build(BuildContext context) {
    return RichText(
      text: TextSpan(
          text: versetNumber + "  ",
          style: GoogleFonts.nunito(
            fontSize: size + 2,
            fontWeight: FontWeight.w900,
            color: primaryText,
            letterSpacing: 2,
          ),
          children: [
            TextSpan(
                text: text,
                style: GoogleFonts.nunito(
                  fontSize: size,
                  fontWeight: FontWeight.w500,
                  color: color,
                  letterSpacing: 2,
                ))
          ]),
    );
  }
}
