import 'package:flutter/material.dart';

// the bible routes navigations strings
const routeBibleBooks = '/bible/$routeBibleBooksPage';
const routeBibleBooksPage = 'books';
const routeBibleChapterList = 'chapter_list';
const routeBibleChapterPage = 'chapter';
const routeBibleSearch = 'search';
const routeBibleSearchDisplay = 'display_search_list';
const routeBibleSearchDisplayItem = 'display_search_item';

class BibleStringArguments {
  final String title;
  final List<String> message;
  final String bookName;
  final int bookColor;

  BibleStringArguments(this.title, this.message, this.bookName, this.bookColor);
}

class BibleVersetsArguments {
  final String title;
  final String message;
  final String bookName;
  final List<String> chaptersIds;
  final int pos;
  final int bookColor;

  BibleVersetsArguments(
      this.title, this.message, this.bookName, this.chaptersIds, this.pos, this.bookColor);
}

class BibleBookChoiceInt {
  final int bookCoor;

  BibleBookChoiceInt(this.bookCoor);
}
