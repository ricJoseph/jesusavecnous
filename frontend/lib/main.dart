import 'package:flutter/material.dart';
import 'bible/bible.dart';
import 'package:frontend/home.dart';

// the bible route navigation prefix
const routeBiblePrefix = '/bible/';
const routeHome = '/';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    onGenerateRoute: (settings) {
      late Widget displayedPage;
      if (settings.name == routeHome) {
        displayedPage = const HomeScreen();
      } else if (settings.name!.startsWith(routeBiblePrefix)) {
        final subRoute = settings.name!.substring(routeBiblePrefix.length);
        displayedPage = BibleFlow(biblePageRoute: subRoute);
      } else {
        throw Exception('Unknow navigation route: ${settings.name}');
      }

      return MaterialPageRoute<dynamic>(
          builder: (context) {
            return displayedPage;
          },
          settings: settings);
    },
  ));
}
