from typing import Any
from manager.utils import get_collection_handle, get_db_handle
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.decorators import api_view, permission_classes
import json
from bson import ObjectId
# Create your views here.

DATABASE_NAME="jan"
DATABASE_HOST="localhost"
DATABASE_PORT=27017
USERNAME="joric2458"
PASSWORD="joric28@Yahoo.2845"
BIBLE_COLLECTION="bible"

#acces the bible collection in data base 
db_handle, mongo_client = get_db_handle(DATABASE_NAME, DATABASE_HOST, DATABASE_PORT, USERNAME,PASSWORD)
collection_handle = get_collection_handle(db_handle, BIBLE_COLLECTION)

class JSONEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ObjectId):
            return str(o)
        return json.JSONEncoder.default(o)

@api_view(['GET'],)
@permission_classes([AllowAny],)
def getPartA(request):
    message = ""
    part1 = collection_handle.find_one({"label":"AT"})
    message = json.dumps(part1, ensure_ascii=True, cls=JSONEncoder)
    return Response({"message": message}, status=200)

@api_view(['GET'],)
@permission_classes([AllowAny],)
def getPartN(request):
    message = ""
    part1 = collection_handle.find_one({"label":"NT"})
    message = json.dumps(part1, ensure_ascii=True, cls=JSONEncoder)
    return Response({"message": message}, status=200)

@api_view(['GET'],)
@permission_classes([AllowAny],)
def getVersets(request):
    idString = request.GET['chapterId']
    idObj = ObjectId(idString)

    message = collection_handle.find_one({"_id": idObj}),
    
    temp = ""

    for versetId in message[0]["versets"]:
        part = collection_handle.find_one({"_id": ObjectId(str(versetId))})
        temp =temp+ ","+ json.dumps(part, ensure_ascii=True, cls=JSONEncoder)
    resp = "["+temp[1:]+"]"
    return Response(resp, status=200)