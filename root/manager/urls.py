from django.urls import path
from .import views

urlpatterns = [
    path('getPartA', views.getPartA, name='getPartA'),
    path('getPartN', views.getPartN, name='getPartN'),
    path('getVersets', views.getVersets, name='getVersets')
]
