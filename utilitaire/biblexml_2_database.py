from lxml import etree
import pymongo
import urllib.parse
from pymongo import MongoClient
from bson import json_util, ObjectId
import json

def parse_json(data):
    return json.loads(json_util.dumps(data))

host = "localhost"
port = 27017

user_name = "joric2458"
pass_word = "joric28@Yahoo.2845"  

db_name = "jan"  # database name to authenticate

# if your password has '@' then you might need to escape hence we are using "urllib.parse.quote_plus()" 
client = MongoClient(f'mongodb://{user_name}:{urllib.parse.quote_plus(pass_word)}@{host}:{port}/') 

db = client["jan"]
colec = db["bible"]

path = "bible.xml"

treeb = etree.parse("bible.xml")

bookLength = 1

bible = {}
ancien_testament = []
nouveau_testament = []

#verset = treeb.xpath('/XMLBIBLE/BIBLEBOOK[@bnumber=1]/CHAPTER')
#for verse in verset:
#    print(verse.get("cnumber"))
for book in treeb.xpath('/XMLBIBLE/BIBLEBOOK'):
    livr = {} 
    if bookLength <= 39 :
        livr['num'] = book.get("bnumber")
        livr['name'] = book.get("bname")
        livr['sname'] = book.get("bsname")
        chap = []
        chapNum = 1
        for cha in treeb.xpath(f'/XMLBIBLE/BIBLEBOOK[@bnumber={bookLength}]/CHAPTER'):
            chapitre = {}
            chapitre['num'] = cha.get("cnumber")
            versets = []
            for verse in treeb.xpath(f'/XMLBIBLE/BIBLEBOOK[@bnumber={bookLength}]/CHAPTER[@cnumber={chapNum}]/VERS'):
                verset = {}
                verset['num'] = verse.get("vnumber")
                verset['data'] = verse.text
                versets.append(colec.insert_one(verset).inserted_id)
            chapNum = chapNum + 1
            chapitre['versets'] = versets
            chap.append(colec.insert_one(chapitre).inserted_id)
        bookLength = bookLength + 1
        livr['chapitres'] = chap
        ancien_testament.append(livr)
    if bookLength > 39 :
        livr['num'] = book.get("bnumber")
        livr['name'] = book.get("bname")
        livr['sname'] = book.get("bsname")
        chap = []
        chapNum = 1
        for cha in treeb.xpath(f'/XMLBIBLE/BIBLEBOOK[@bnumber={bookLength}]/CHAPTER'):
            chapitre = {}
            chapitre['num'] = cha.get("cnumber")
            versets = []
            for verse in treeb.xpath(f'/XMLBIBLE/BIBLEBOOK[@bnumber={bookLength}]/CHAPTER[@cnumber={chapNum}]/VERS'):
                verset = {}
                verset['num'] = verse.get("vnumber")
                verset['data'] = verse.text
                versets.append(colec.insert_one(verset).inserted_id)
            chapNum = chapNum + 1
            chapitre['versets'] = versets
            chap.append(colec.insert_one(chapitre).inserted_id)
        bookLength = bookLength + 1
        livr['chapitres'] = chap
        nouveau_testament.append(livr)    

ancien = {}
nouveu = {}
ancien["label"] = "AT"
nouveu["label"] = "NT"
ancien["at"] = ancien_testament
nouveu["nt"] = nouveau_testament

temps= []

temps.append(colec.insert_one(ancien).inserted_id)
temps.append(colec.insert_one(nouveu).inserted_id)


bible["name"] = "bible"
bible["part"] = temps

colec.insert_one(bible)